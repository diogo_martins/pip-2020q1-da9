#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Calcula justos compostos dado o capital, a taxa e o tempo
"""

capital = float(input("Capital (R$): "))
taxa = float(input("Taxa (% a.m.): "))
tempo = float(input("Tempo (meses): "))

total = capital * (1 + taxa / 100) ** tempo
juros = total - capital

print(f"Juros (R$): {juros:.2f}")
print(f"Total a pagar (R$): {total:.2f}")