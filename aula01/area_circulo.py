#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Calcula a area do circulo com base no raio
"""

import math

raio = float(input("Raio (cm): "))

area = math.pi * raio * raio

print(f"Area: {area:.3f} cm**2")
