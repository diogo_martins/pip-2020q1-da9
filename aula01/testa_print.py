#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Testa variacoes de prints e strings formatadas
"""

x = 10
y = 20
z = x + y

print("x = " + str(x))
print(f"y = {y}")
print("x + y =", z)
print("x * y = %d" % (x * y))

