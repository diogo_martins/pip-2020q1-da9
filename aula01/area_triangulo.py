#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Calcula a area do triangulo dados a base e a altura
"""

base = float(input("Base (cm): "))
altura = float(input("Altura (cm): "))
area = base * altura / 2

print(f"Area: {area} cm**2")

