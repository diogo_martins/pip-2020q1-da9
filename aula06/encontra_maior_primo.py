# Encontra o maior número primo que seja menor do que n > 0

from primalidade import is_prime3

n = int(input('n: '))

for i in range(n-1, 1, -1):
    if is_prime3(i):
        print(i)
        break

