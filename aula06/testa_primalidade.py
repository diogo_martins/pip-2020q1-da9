from primalidade import is_prime3
from primalidade import is_prime2
from primalidade import is_prime

print(is_prime(0))
print(is_prime(1))
print(is_prime(2))
print(is_prime(3))
print(is_prime(4))

print()

print(is_prime2(0))
print(is_prime2(1))
print(is_prime2(2))
print(is_prime2(3))
print(is_prime2(4))

print()

print(is_prime3(0))
print(is_prime3(1))
print(is_prime3(2))
print(is_prime3(3))
print(is_prime3(4))

