
'''
Determina o valor maximo da matriz de reais
'''
def maximo_matriz(matriz: [[float]]) -> float:
    if len(matriz) < 1.0:
        return None
    
    maximo = matriz[0][0]
    
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            if matriz[i][j] > maximo:
                maximo = matriz[i][j]
    
    return maximo

'''
Determina a qtde de digitos de um inteiro
'''
def conta_digitos(n: int) -> int:
    if n == 0:
        return 1
    
    c = 0
    while n > 0:
        n = n // 10
        c = c + 1
        
    return c
    

'''
Determina o maior numero dentre os armazenados 
na matriz e retorna a quantidade de digitos desse
numero
'''
def valor_mais_extenso_int(matriz: [[int]]) -> int:
    return conta_digitos(maximo_matriz(matriz))


'''
Determina o maior numero dentro os armazenados na 
matriz e retorna a quantidade de digitos desse numero
'''
def valor_mais_extenso_real(matriz: [[float]]) -> int:
    maximo = int(maximo_matriz(matriz))
    
    return conta_digitos(maximo)
    




