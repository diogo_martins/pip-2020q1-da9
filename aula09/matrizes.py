'''
Biblioteca com operações fundamentais sobre matrizes
'''

import random

'''
Cria uma matriz de inteiros de n linhas e m colunas
'''
def cria_matriz_zeros_int(n: int, m: int) -> [[int]]:
    matriz = []
    for i in range(n):
        matriz.append([])
        for j in range(m):
            matriz[i].append(0)
            
    return matriz

'''
Imprime uma matriz de tamanho arbitrario no 
formato retangular, sendo que cada celula
ocupa lcol posicoes
'''
def imprime_matriz_int(matriz: [[int]], lcol: int = 3) -> None:
    for i in range(len(matriz)):
        for j in range(len(matriz[0])):
            #print(matriz[i][j], end=' ')
            print(f'{matriz[i][j]:{lcol}}', end='')
        print()

'''
Cria uma matriz, de n linhas com m colunas, 
cujos elementos sao inteiros aleatorios 
variando de minimo a maximo
'''
def cria_matriz_aleatoria_int(n: int, m: int, 
                              minimo: int = 0, 
                              maximo: int = 100) -> [[int]]:
    matriz = []
    
    for i in range(n):
        matriz.append([])
        for j in range(m):
            r = random.randint(minimo, maximo)
            matriz[i].append(r)
            
    return matriz

'''
Cria uma matriz, de n linhas com m colunas, 
cujos elementos sao reais aleatorios
variando de minimo a maximo
'''
def cria_matriz_aleatoria_real(n: int, m: int, 
                               minimo: float = 0.0,
                               maximo: float = 100.0) -> [[float]]:
    matriz = []
    
    for i in range(n):
        matriz.append([])
        for j in range(m):
            matriz[i].append(random.uniform(minimo,maximo))
    
    return matriz

'''
Imprime uma matriz de numeros reais, considerando
que cada numero real esta formatado com ncasas casas
decimais na parte fracionaria e o cada celula da matriz
ocupa lcol posicoes
'''
def imprime_matriz_real(m: [[float]], ncasas: int = 3, 
                        lcol: int = 7) -> None:
    for i in range(len(m)):
        for j in range(len(m[0])):
            print(f'{m[i][j]:{lcol}.{ncasas}f}', end='')
        print()
        
        
'''
Determina o valor maximo da matriz de reais
'''
def maximo_matriz(matriz: [[float]]) -> float:
    if len(matriz) < 1.0:
        return None
    
    maximo = matriz[0][0]
    
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            if matriz[i][j] > maximo:
                maximo = matriz[i][j]
    
    return maximo

'''
Determina a qtde de digitos de um inteiro
'''
def conta_digitos(n: int) -> int:
    if n == 0:
        return 1
    
    c = 0
    while n > 0:
        n = n // 10
        c = c + 1
        
    return c
    

'''
Determina o maior numero dentre os armazenados 
na matriz e retorna a quantidade de digitos desse
numero
'''
def valor_mais_extenso_int(matriz: [[int]]) -> int:
    return conta_digitos(maximo_matriz(matriz))


'''
Determina o maior numero dentro os armazenados na 
matriz e retorna a quantidade de digitos desse numero
'''
def valor_mais_extenso_real(matriz: [[float]]) -> int:
    maximo = int(maximo_matriz(matriz))
    
    return conta_digitos(maximo)

'''
Imprime uma matriz de inteiro no formato retangular
Determina automaticamente a largura maxima entre as 
colunas da matriz
'''
def formata_autom_matriz_int(matriz: [[int]]) -> None:
    lcol = valor_mais_extenso_int(matriz) + 1
    
    imprime_matriz_int(matriz, lcol)

# Exercicio 1: criar uma funcao que determina a largura maxima
# entre todas as colunas de uma matriz de reais. A maior largura
# de coluna de uma matriz de reais corresponde ao espaco necessario
# para exibir o numero mais extenso da matriz. Lembre-se que
# o espaço ocupado por um numero equivale a qtde de digitos
# da parte inteira, mais a qtde de digitos da parte fracionaria,
# mais um espaco correspondente ao ponto.

# Exercicio 2: criar uma funcao que determina a largura otima
# de cada coluna. A largura otima de uma coluna corresponde
# a qtde de digitos do maior elemento da respectiva coluna.
# Utilize essa definicao para criar uma funcao que imprima
# a matriz utilizando a largura otima de cada coluna


def main():
    # codigos de teste
    #print(cria_matriz_zeros_int(2,2))
    print(cria_matriz_zeros_int(3,4))
    
if __name__ == '__main__':
    main()
            