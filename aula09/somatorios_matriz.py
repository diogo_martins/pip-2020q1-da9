
'''
Retorna um vetor contendo os somatorios
das respectivas linhas da matriz, sendo
que o i-esimo elemento do vetor corresponde
ao somatorio da i-esima linha da matriz
'''
def soma_linhas(matriz: [[int]]) -> [int]:
    somas = [0] * len(matriz)
    
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            somas[i] += matriz[i][j]
            
    return somas

'''
Retorna um vetor contendo os somatorios
das respectivas colunas da matriz, sendo
que o j-esimo elemento do vetor de somas 
corresponde ao somatorio da j-esima coluna
da matriz
'''
def soma_colunas(matriz: [[int]]) -> [int]:
    qtde_linhas = len(matriz)
    
    if qtde_linhas < 1:
        return []    
    
    qtde_colunas = len(matriz[0])
    somas = [0] * qtde_colunas
    
    for j in range(qtde_colunas):
        for i in range(qtde_linhas):
            somas[j] += matriz[i][j]
            
    return somas

'''
versao alternativa do somatorio das colunas,
porem empregando percurso por linhas
'''
def soma_colunas_2(matriz: [[int]]) -> [int]:
    qtde_linhas = len(matriz)
    
    if qtde_linhas < 1:
        return []    
    
    qtde_colunas = len(matriz[0])
    somas = [0] * qtde_colunas
    
    for i in range(qtde_linhas):
        for j in range(qtde_colunas):
            somas[j] += matriz[i][j]
            
    return somas
            
