'''
Particiona os valores de v em dois vetores, um para pares
e outro para impares, com a restricao de que ambos os
vetores auxiliares tem tamanho 3.
'''

def imprime_pares(vp):
    for i in range(len(vp)):
        print(f'par[{i}] = {vp[i]}')

def imprime_impares(vi):
    for i in range(len(vi)):
        print(f'impar[{i}] = {vi[i]}')

n = int(input())

# Exercício
# Considere v = [0] * n, impar = [0,0,0] e par = [0,0,0]
# Com base nessas definicoes, refaça o algoritmo sem usar
# append() e clear()
v = []
impar = []
par =  []

for i in range(n):
    v.append(int(input()))
    
for i in range(n):
    if v[i] % 2 == 0:
        if len(par) == 3:
            imprime_pares(par)
            par.clear()
        par.append(v[i])
    else:
        if len(impar) == 3:
            imprime_impares(impar)
            impar.clear()
        impar.append(v[i])
        
imprime_impares(impar)
imprime_pares(par)
        
    
        