'''
Preenche um vetor de tamanho n com os dobros sucessivos
a partir do primeiro elemento
'''

n = int(input())
k = int(input())
v = [0] * n

if n > 0:
    v[0] = k

for i in range(1,n):
    v[i] = 2 * v[i-1]
    

for i in range(n):
    print(f'v[{i}] = {v[i]}')
