'''
gera os k primeiros elementos da sequencia de fibonacci
abordagem com a lista crescendo incrementalmente
'''
def fibonacci(k: int) -> [int]:    
    if k == 0:
        return []
    if k == 1:
        return [0]
    
    v = [0,1]
    
    for i in range(2,k):
        v.append(v[len(v)-1] + v[len(v)-2])
        
    return v

'''
gera os k primeiros elementos da sequencia de fibonacci
abordagem: a lista eh dimensionada no inicio do processamento
'''
def fibonacci2(k: int) -> [int]:
    v = [0] * k
    
    if k == 0 or k == 1:
        return v 
    
    v[1] = 1
    for i in range(2,k):
        v[i] = v[i-1] + v[i-2]
        
    return v

'''
gera os primeiros elementos de fibonacci que são menores
ou iguais a n
abordagem: fazer o vetor crescer incrementalmente ateh 
atingir o limite superior (n)
'''
def fibonacci_menor_igual_que(n: int) -> [int]:    
    if n == 0:
        return [0]
    if n == 1:
        return [0,1,1]
    
    v = [0,1,1]
    
    while True:
        c = v[len(v)-1] + v[len(v)-2]
        if c <= n:
            v.append(c)
        else:
            break
        
    return v

'''
Retorna o conteudo revertido da lista  v
'''
def reverte_lista(v: [int]) -> [int]:
    vr = []
    
    for i in range(len(v)-1, -1, -1):
        vr.append(v[i])
        
    return vr

'''
Gera a sequencia reversa (decrescente) contendo os 
elementos de fibonacci menores ou iguais a n
'''
def fib_rev_menor_igual_que(n):
    return reverte_lista(fibonacci_menor_igual_que(n))
    

for i in range(30):
    print(i, fib_rev_menor_igual_que(i))

