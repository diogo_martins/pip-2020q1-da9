# '''
# testa se b eh substring de a
# utilizando while
# '''
# def testa_substring(a: str, b: str) -> bool:
#     if len(b) > len(a):
#         return False
#     if len(b) == 0:
#         return True
#     i = 0
#     while i < len(a):
#         j = 0
#         while i+j < len(a) and j < len(b):
#             if a[i+j] != b[j]:
#                 break
#             j += 1
#         if j == len(b):
#             return True
#         i += 1
    
#     return False

'''
testa se b eh substring de a
utilizando for
'''
def testa_substring(a, b):
    if len(b) > len(a):
        return False
    if len(b) == 0:
        return True
    
    for i in range(len(a)):
        for j in range(len(b)):
            if i + j >= len(a):
                break
            if a[i+j] != b[j]:
                break
            if j == len(b) - 1:
                return True
    
    return False   
    

print(testa_substring('abc', ''), '')

print(testa_substring('abc', 'a'), 'a')
print(testa_substring('abc', 'b'), 'b')
print(testa_substring('abc', 'c'), 'c')

print(testa_substring('abc', 'ab'), 'ab')
print(testa_substring('abc', 'ac'), 'ac')
print(testa_substring('abc', 'ba'), 'ba')
print(testa_substring('abc', 'bc'), 'bc')
print(testa_substring('abc', 'ca'), 'ca')
print(testa_substring('abc', 'cb'), 'cb')

print(testa_substring('abc', 'abc'), 'abc')
print(testa_substring('abc', 'acb'), 'acb')
print(testa_substring('abc', 'bac'), 'bac')
print(testa_substring('abc', 'bca'), 'bca')
print(testa_substring('abc', 'cab'), 'cab')
print(testa_substring('abc', 'cba'), 'cba')