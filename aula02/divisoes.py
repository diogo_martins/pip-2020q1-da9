#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Lê dois inteiros e simula dois cenários de divisibilidade
entre eles
"""

a = int(input("a: "))
b= int(input("b: "))

print()
print(f"Dividendo: {a}")
print(f"Divisor:   {b}")
print(f"Quociente: {a // b}")
print(f"Resto:     {a % b}")

print()
print(f"Dividendo: {b}")
print(f"Divisor:   {a}")
print(f"Quociente: {b // a}")
print(f"Resto:     {b % a}")