#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Simula uma calculadora de operações aritméticas
fundamentais
"""

a = float(int(input("a: ")))
b = float(int(input("b: ")))
operador = input("Operador: ")

if operador == "+":
    c = a + b
if operador == "-":
    c = a - b
if operador == "*":
    c = a * b
if operador == "/":
    c = a / b
    
print(f"{a:.1f} {operador} {b:.1f} = {c:.1f}")
